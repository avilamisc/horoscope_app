package com.example.horoscope_app

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.only_line.view.*
import java.util.*
import kotlin.collections.ArrayList

class HoroscopeBaseAdapter(context:Context,allHoroscope:ArrayList<HoroscopesData>):BaseAdapter() {

    var allHoroscope:ArrayList<HoroscopesData>
    var context: Context

    init {
      this.allHoroscope= allHoroscope
        this.context=context
//
//        var horoscopeName=context.resources.getStringArray(R.array.horoscopeArr) //12 tane emin ol (2 side esit olmali yoksa java.lang.ArrayIndexOutOfBoundsException: length=11; index=11 at com.example.horoscope_app.horoscopeArrayAdapter.getView alirsin )
//        var horoscopeDate=context.resources.getStringArray(R.array.horoscopeDate) //12 tane emin ol
//
//        var horoscopeSymbol= arrayOf(R.drawable.koc1,R.drawable.boga2,R.drawable.ikizler3,R.drawable.yengec4,R.drawable.aslan5,R.drawable.basak6,
//            R.drawable.terazi7,R.drawable.akrep8,R.drawable.yay9,R.drawable.oglak10,R.drawable.kova11,R.drawable.balik12)//12 tane emin ol
//
//        for(i in 0..11){ // eleman sayi varsayalim 11 olsun
//            var arrayListData=HoroscopesData(horoscopeName[i],horoscopeDate[i],horoscopeSymbol[i])
//            allHoroscope.add(arrayListData)
//        }

    }

    //daha once urettigimiz viewlari tekrar tekrar kullanmaya yarar

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        //herbir satiri olustururken tetiklenen fonksiyon

        var only_line_view=convertView
        var viewHolder:ViewHolderA
        if(only_line_view==null){
            var inflater=LayoutInflater.from(context)
            only_line_view= inflater.inflate(R.layout.only_line,parent, false)
        viewHolder= ViewHolderA(only_line_view)
            only_line_view.tag=viewHolder // only_line_view ile ilişkilendiriyorum(veri gibi atiyorum)


        }else{
            viewHolder=only_line_view.getTag() as ViewHolderA //attigim veriyi koruyorum
        }


        viewHolder.imgHorScorpionA.setImageResource(allHoroscope[position].horoscopeSymbol)

        viewHolder.txtViewHoroscopeA.txtViewHoroscope.setText(allHoroscope[position].horoscopeName)


        viewHolder.txtHoroscopeDateA.setText(allHoroscope[position].horoscopeDate)

        return only_line_view

    }

    override fun getItem(position: Int): Any {
            //ilgili satirdaki eleman nasıl ulasilacagi hakkında bilgi veren method
        return allHoroscope.get(position)
    }

    override fun getItemId(position: Int): Long {
        //veritabaninlarinda kullanilan method


        //veritabanlarinda kullanildiginda simdilik bos geciyoruz
        return 0
    }

    override fun getCount(): Int {
            //liste kac elemanlı oldugu

        return allHoroscope.size
    }

}

class ViewHolderA(only_line_viewB:View){
    var imgHorScorpionA: ImageView
    var txtViewHoroscopeA: TextView
    var txtHoroscopeDateA:TextView

    init {
        this.imgHorScorpionA=only_line_viewB.imgHorScorpion
        this.txtHoroscopeDateA=only_line_viewB.txtHoroscopeDate
        this.txtViewHoroscopeA=only_line_viewB.txtViewHoroscope
    }


}
package com.example.horoscope_app

import java.io.Serializable

data class HoroscopesData(var horoscopeName: String,var horoscopeDate:String,var horoscopeSymbol:Int,var bighoroscopeSymbol:Int,var generalFeaturesHoroscope:String ):Serializable {
}
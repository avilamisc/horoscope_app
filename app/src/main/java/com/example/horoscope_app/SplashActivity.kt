package com.example.horoscope_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    /*
     NOT:
     Kullanıcı suan uygulaayla etkilisim halindeyse onResume() tektiklenir,CountDownTimer geri gittigndede tekrar calismasi icin gerekli

     */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        var logoRotation=AnimationUtils.loadAnimation(this,R.anim.logo_rotating)
        imgLogo.animation=logoRotation




    }

    override fun onResume() {
        //timerdan sonra sayfa yonlendirebilirisin
        object:CountDownTimer(5000,1000){

            override fun onFinish() { //5sn bitikten sonra burasi
                var intent= Intent(this@SplashActivity,MainActivity::class.java)//splash ten main activiy gececek
                startActivity(intent) // istenilen activitye gonder

            }

            override fun onTick(millisUntilFinished: Long) { //her 1 sn burasi

            }
        }.start()

        super.onResume()
    }
}
